bool anyUserUnder18(Iterable<User> users) {
  return users.any((user) => user.age < 18);
}

bool everyUserOver13(Iterable<User> users) {
  return users.every((user) => user.age > 13);
}

Iterable<User> filterOutUnder21(Iterable<User> users) {
  return users.where((user) => user.age >= 21);
}

Iterable<User> findShortNamed(Iterable<User> users) {
  return users.where((user) => user.name.length <= 3);
}

Iterable<String> getNameAndAges(Iterable<User> users) {
  return users.map((user) => '${user.name} is ${user.age}');
}

void main() {
  var users = [
    User(name: 'ABCDE', age: 14),
    User(name: 'BCDEF', age: 15),
    User(name: 'CDE', age: 16),
    User(name: 'DEF', age: 21),
    User(name: 'E', age: 25),
  ];
  if (anyUserUnder18(users)) {
    print('Have any user under 18');
  }
  if (everyUserOver13(users)) {
    print('Every users are over 13');
  }
  var ageMoreThan21Users = filterOutUnder21(users);
  print('Age more than 21:');
  for (var user in ageMoreThan21Users) {
    print(user);
  }

  var shortNameUser = findShortNamed(users);
  print('shorted Name:');
  for (var user in shortNameUser) {
    print(user);
  }

  var NameAndAge = getNameAndAges(users);
  print('Name and Age:');
  for (var user in NameAndAge) {
    print(user);
  }
}

class User {
  String name;
  int age;
  User({required this.name, required this.age});
  @override
  String toString() {
    return '$name, $age';
  }
}
