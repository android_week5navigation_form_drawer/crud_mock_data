import 'package:iterable/iterable.dart' as iterable;

void main(List<String> arguments) {
  Iterable<int> iterable = [1, 2, 3];
  int value = iterable.elementAt(1);
  print(value);

  //List Name
  const foods = ['Salad', 'Popcorn', 'Toast', 'Lasagne'];
  Iterable<String> iterableFoods = foods;
  for (var food in iterableFoods) {
    print(food);
  }

  print('First element is ${foods.first}');
  print('Last element is ${foods.last}');

  var foundItem = foods.firstWhere((element) {
    return element.length > 5;
  });
  print(foundItem);

  var foundItem2 = foods.firstWhere((element) => element.length > 5);
  print(foundItem2);

  bool predicate(String item) {
    return item.length > 5;
  }

  var foundItem3 = foods.firstWhere(predicate);
  print(foundItem3);

  var foundItem4 = foods.firstWhere(
    (item) => item.length > 10,
    orElse: () => 'None!',
  );
  print(foundItem4);

  //Find to frist word
  var foundItem5 = foods.firstWhere(
      (item) => item.startsWith('M') && item.contains('a'),
      orElse: () => 'None!');
  print(foundItem5);

  //if is check case
  if (foods.any((item) => item.contains('a'))) {
    print('At least one item contains "a"');
  }
  if (foods.every((item) => item.length >= 5)) {
    print('All items have length >= 5');
  }

  var numbers = const [1, -2, 3, 42, 0, 4, 5, 6];
  var evenNumbers = numbers.where((number) => number.isEven);
  //Find to number in list
  for (var number in evenNumbers) {
    print('$number is even.');
  }
  //check number in list is negative
  if (evenNumbers.any((number) => number.isNegative)) {
    print('evenNumbers contains negative numbers.');
  }
  //find the Maximum
  var largeNumbers = evenNumbers.where((number) => number > 1000);
  if (largeNumbers.isEmpty) {
    print('largeNumbers is empty!');
  }
  //Loop list if find 0 Stop
  var numbersUntilZero = numbers.takeWhile((number) => number != 0);
  print('Numbers until 0: $numbersUntilZero');
  //Keep 0 push in loop list
  var numbersStartingAtZero = numbers.skipWhile((number) => number != 0);
  print('Numbers starting at 0: $numbersStartingAtZero');

  var numbersByTwo = numbers.map((number) => number * 2);
  print('Numbers: $numbersByTwo');
}
