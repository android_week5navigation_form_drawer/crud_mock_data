import 'package:dog_mockdata/dog.dart';
import 'dog.dart';

var lastId = 3;
var mockDog = [Dog(id: 1, name: 'A', age: 8), Dog(id: 2, name: 'B', age: 5)];

int getNewId() {
  return lastId++;
}

Future<void> addNewDog(Dog dog) {
  return Future.delayed(Duration(seconds: 1), () {
    mockDog.add(Dog(id: getNewId(), name: dog.name, age: dog.age));
  });
}

Future<void> saveDog(Dog dog) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockDog.indexWhere((item) => item.id == dog.id);
    mockDog[index] = dog;
  });
}

Future<void> delDog(Dog dog) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockDog.indexWhere((item) => item.id == dog.id);
    mockDog.removeAt(index);
  });
}

Future<List<Dog>> getDogs() {
  return Future.delayed(Duration(seconds: 1), () => mockDog);
}
